﻿using System;

namespace Fundacion.Jala.Calculator.Exception
{
    public class ExceptionHandler: SystemException
    {
        public ExceptionHandler(string name)
        : base(String.Format(name))
        {
        }
    }
}

﻿using System;

namespace Fundacion.Jala.RomanNumbers.RomanNumber
{
    public class RomanNumber
    {
        public string Value { set; get;}
        public int Number { set; get;}

        public RomanNumber(int number, string value)
        {
            Number = number;
            Value = value;
        }

        public void ShowRomanNumber()
        {
            Console.WriteLine($"Arabig Number: {Number}");
            Console.WriteLine($"Roman Number: {Value}");
        }
    }
}

﻿using System.Text;

namespace Fundacion.Jala.DevInt.Practice2.Store.Models
{
    public class GameController : ProductBase
    {
        public bool IsWireless { get; set; }
        public override string FullName => GetName();
        public string GetName()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Brand).Append("-").Append(Model);
            return builder.ToString();
        }
    }
}

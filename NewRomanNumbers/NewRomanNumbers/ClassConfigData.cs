﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRomanNumbers
{
    public class ClassConfigData
    {
        public bool IsUpperCase { set; get; }
        public bool HaveBrackets { set; get; }
        private NameValueCollection values;
        public ClassConfigData()
        {
            
        }

        public void ConfigurationAppSettings()
        {
            values = ConfigurationManager.AppSettings;
            if (Convert.ToBoolean(values["uppercase"]))
            {
                IsUpperCase = true;
            }
            else
            {
                IsUpperCase = false;
            }
            if (Convert.ToBoolean(values["brackets"]))
            {
                HaveBrackets = true;
            }
            else
            {
                HaveBrackets = false;
            }
        }

        public List<string> GetProperties()
        {
            List<string> properties = new List<string>();
            if (HaveBrackets)
            {
                properties.Add("[");
                properties.Add("]");
            }
            return properties;
        }
    }
}

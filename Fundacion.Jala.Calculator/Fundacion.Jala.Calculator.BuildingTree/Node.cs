﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fundacion.Jala.Calculator.BuildingTree
{
    public class Node
    {
        public char Value;
        public Node LeftData, RightData;

        public Node(char newData)
        {
            Value = newData;
            LeftData = null;
            RightData = null;
        }
    }
}

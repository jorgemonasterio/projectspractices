﻿using Fundacion.Jala.DevInt.Practice2.Store;
using Fundacion.Jala.DevInt.Practice2.Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            StockManager _stockManager;
            Monitor _monitor;
            GameController _gameControllerXBox;
            GameController _gameControllerPS;

            _stockManager = new StockManager();
            _monitor = new Monitor()
            {
                Brand = "Samsung",
                Model = "SR-75",
                Inches = 32
            };
            _gameControllerXBox = new GameController()
            {
                Brand = "Xbox",
                Model = "Sport RSE"
            };
            _gameControllerPS = new GameController()
            {
                Brand = "PlayStation",
                Model = "MarioKart Edition"
            };

            _stockManager.Add(_monitor, 2);
            _stockManager.Add(_gameControllerXBox, 5);
            _stockManager.Add(_gameControllerPS, 4);
            _stockManager.GetProductsBy(nameof(GameController));
            Console.ReadKey();
        }
    }
}

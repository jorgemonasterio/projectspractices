﻿using System;

namespace practice
{
    class Program
    {
        static int[] chargeValues(string value)
        {
            string[] words = value.Split(',');
            int [] arr = new int[2];
            int i = 0;
            foreach (string word in words)
            {
                arr[i] = int.Parse(word);
                i++;
            }
            return arr;
        }

        static void sumArrays(int[] array1, int[] array2)
        {
            int[] newArray = new int[2];
            for (int i = 0, j = 0; i < newArray.Length; i++, j++)
            {
                newArray[i] = array1[i] + array2[j];                             
            }
            for (int i = 0; i < newArray.Length; i++)
            {
                Console.Write(newArray[i] + " ");
            }
        }

        static void Main(string[] args)
        {
            int [] array1 = chargeValues(args[0]);
            int [] array2 = chargeValues(args[1]);
            sumArrays(array1,array2);
        }
    }
}

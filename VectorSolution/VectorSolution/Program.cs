﻿using System;

namespace VectorSolution.NetFrameworkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] args1 = new string[2] { "2,1", "-3,5" };
            var vector2D = new VectorBuilder();
            var vector1 = vector2D.ConstructVector(args1[0]);
            var vector2 = vector2D.ConstructVector(args1[1]);
            var resultSum = vector1.SumOperation(vector2);
            var resultDot = vector1.DotOperation(vector2);
            var lengthVector1 = vector1.LenghtVector();
            var lengthVector2 = vector2.LenghtVector();
            Console.WriteLine("After operations:");
            Console.WriteLine($"Sum: {resultSum.ToString()}, Dot: {resultDot}, " +
                $"Length Vector1 : {lengthVector1}, Length Vector2 : {lengthVector2}");
            Console.ReadKey();
        }
    }
}

﻿using Fundacion.Jala.Calculator.Exception;
using Fundacion.Jala.Calculator.InputValidator;
using System;
using System.Collections;
using System.Text;

namespace Fundacion.Jala.Calculator.PostFixCoverter
{
    public class PostFixConverter
    {
        Stack analyceValues;
        Queue finalSolution;
        public PostFixConverter()
        {
            this.analyceValues = new Stack();
            this.finalSolution = new Queue();
        }
        public string GenerateFinalPostFix(string value)
        {
            Validator val = new Validator();
            var tam = 0;
            while (tam!=value.Length)
            {
                if (val.correctFormatExpression(value))
                {
                    foreach (var character in value)
                    {
                        switch (Convert.ToInt32(character))
                        {
                            case 40: // (
                                analyceValues.Push(character);
                                tam++;
                                break;
                            case 41: // )
                                PopAnalizeStack();
                                tam++;
                                break;
                            case 42: // *
                                analyceValues.Push(character);
                                tam++;
                                break;
                            case 43: //+
                                if (analyceValues.Contains('*') == true || analyceValues.Contains('/') == true)
                                {
                                    while ( analyceValues.Count!=0 && (Convert.ToChar(analyceValues.Peek()) == '*' || Convert.ToChar(analyceValues.Peek()) == '*'))
                                    {
                                            var newValue = Convert.ToChar(analyceValues.Pop());
                                            finalSolution.Enqueue(newValue);
                                    }
                                    analyceValues.Push(character);
                                }
                                else
                                {
                                    analyceValues.Push('+');
                                }
                                tam++;
                                break;
                            case 45: // -
                                if (analyceValues.Contains('*') == true || analyceValues.Contains('/') == true)
                                {
                                    while (analyceValues.Count != 0 && Convert.ToChar(analyceValues.Peek()) == '*')
                                    {
                                        var newValue = Convert.ToChar(analyceValues.Pop());
                                        finalSolution.Enqueue(newValue);
                                    }
                                    analyceValues.Push(character);
                                }
                                else
                                {
                                    analyceValues.Push('-');
                                }
                                tam++;
                                break;
                            case 47: // /
                                analyceValues.Push(character);
                                tam++;
                                break;
                            default:
                                finalSolution.Enqueue(character);
                                tam++;
                                break;
                        }
                    }
                }
                else
                {
                    throw new ExceptionHandler("The format of expression mathematics is incorrect");
                }
            }
            var postFix = GeneratePostFixQueue();
            return postFix;
        }
        public void ShowStack()
        {
            foreach (var item in analyceValues)
            {
                Console.WriteLine($"stack:{item}");
            }
            foreach (var item2 in finalSolution)
            {
                Console.WriteLine($"queue:{item2}");
            }
        }
        public void PopAnalizeStack()
        {
            char value = ' ';
            while (analyceValues.Count != 0)
            {
                value = Convert.ToChar(analyceValues.Pop());
                if (value == '(') { break; }
                finalSolution.Enqueue(value);
            }
        }
        public string GeneratePostFixQueue()
        {
            StringBuilder builder = new StringBuilder();
            if (analyceValues.Count == 0)
            {
                foreach (var item in finalSolution)
                {
                    builder.Append(item);
                }
                return builder.ToString();
            }
            else
            {
                while (analyceValues.Count != 0)
                {
                    var value = analyceValues.Pop();
                    
                    finalSolution.Enqueue(value);
                }
                foreach (var item in finalSolution)
                {
                    builder.Append(item);
                }
                return builder.ToString();
                
            }
        }
    }
}

﻿using System.Text;

namespace Fundacion.Jala.DevInt.Practice2.Store.Models
{
    public class Monitor : ProductBase
    {
        public decimal Inches { get; set; }
        public override string FullName => GetName();
        
        public string GetName()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Inches).Append("'").Append(" ").Append(Brand).Append("-").Append(Model);
            return builder.ToString();
        }
    }
}

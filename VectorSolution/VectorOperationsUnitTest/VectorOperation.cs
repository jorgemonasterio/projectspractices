using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VectorClassLibrary.NetFramework;
using VectorSolution;

namespace VectorOperationsUnitTest
{
    [TestClass]
    
    public class VectorOperation
    {
        #region PositiveTest
        [TestMethod]
        public void TestSumOperation()
        {
            var vector1 = new Vector(2,1);
            var vector2 = new Vector(-3,5);
            var expected = new Vector(-1, 6);
            var result = vector1.SumOperation(vector2);
            Assert.AreEqual(expected.getXcoordenate(), result.getXcoordenate());
            Assert.AreEqual(expected.getYcoordenate(), result.getYcoordenate());
        }
        [TestMethod]
        public void TestDotOperation()
        {
            var vector1 = new Vector(2, 1);
            var vector2 = new Vector(-3, 5);
            var expected = -1;
            var result = vector1.DotOperation(vector2);
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestLengthOfVector()
        {
            var vector1 = new Vector(2, 1);
            var vector2 = new Vector(-3, 5);
            var expected = 2;
            var result = vector1.LenghtVector();
            var result1 = vector2.LenghtVector();
            Assert.AreEqual(expected, result);
        }
        #endregion
        #region NegativeTest
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestInput()
        {
            var vector2D = new VectorBuilder();
            var vector1 = vector2D.ConstructVector("hola mundo");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestInputNumber()
        {
            var vector2D = new VectorBuilder();
            var vector1 = vector2D.ConstructVector("2.1,2");
        }

        #endregion
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;

namespace NewRomanNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new RomanBuilder();
            try
            {
                var newNumber = builder.GetRoman(2749);
                Console.WriteLine(newNumber.ShowRomanNumber());
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
           
            Console.ReadKey();
        }
    }
}

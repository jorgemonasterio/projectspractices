using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewRomanNumbers;
using System;

namespace TestRomanNumbers
{
    [TestClass]
    public class RomanNumberTest
    {
        [TestMethod]
        public void TestGeneratorOfRomanNumbers()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true,
            };
            var builder = new RomanBuilder(data);
            var expected = "MCMXLIX";
            RomanNumber result = builder.GetRoman(1949);
            Assert.AreEqual(expected, result.Symbol);
        }

        [TestMethod]
        public void TestGeneratorOfRomanNumbers2()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            var expected = "MMMCMXCIX";
            RomanNumber result = builder.GetRoman(3999);
            Assert.AreEqual(expected, result.Symbol);
        }

        [TestMethod]
        public void TestGeneratorOfRomanNumbers3()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            var expected = "DXLVII";
            RomanNumber result = builder.GetRoman(547);
            Assert.AreEqual(expected, result.Symbol);
        }

        [TestMethod]
        public void TestGeneratorOfRomanNumbers4()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            var expected = "CMLXXIV";
            RomanNumber result = builder.GetRoman(974);
            Assert.AreEqual(expected, result.Symbol);
        }

        [TestMethod]
        public void TestGeneratorOfRomanNumbers5()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            var expected = "DCLIX";
            RomanNumber result = builder.GetRoman(659);
            Assert.AreEqual(expected, result.Symbol);
        }

        [TestMethod]
        public void Test_Show_A_Roman_Number_As_Uppercase_WithOut_Brackets()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true,
                HaveBrackets = false
            };
            var builder = new RomanBuilder(data);
            var expected = "roman number: CMLXXIV";
            RomanNumber result = builder.GetRoman(974);
            Assert.AreEqual(expected, result.ShowRomanNumber());
        }

        [TestMethod]
        public void Test_Show_A_Roman_Number_As_LowerCase_WithOut_Brackets()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = false,
                HaveBrackets = false
            };
            var builder = new RomanBuilder(data);
            var expected = "roman number: cmlxxiv";
            RomanNumber result = builder.GetRoman(974);
            Assert.AreEqual(expected, result.ShowRomanNumber());
        }

        [TestMethod]
        public void Test_Show_A_Roman_Number_As_LowerCase_With_Brackets()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = false,
                HaveBrackets = true
            };
            var builder = new RomanBuilder(data);
            var expected = "roman number: [cmlxxiv]";
            RomanNumber result = builder.GetRoman(974);
            Assert.AreEqual(expected, result.ShowRomanNumber());
        }

        [TestMethod]
        public void Test_Show_A_Roman_Number_As_UpperCase_With_Brackets()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true,
                HaveBrackets = true
            };
            var builder = new RomanBuilder(data);
            var expected = "roman number: [CMLXXIV]";
            RomanNumber result = builder.GetRoman(974);
            Assert.AreEqual(expected, result.ShowRomanNumber());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestInputIntNumber()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            RomanNumber result = builder.GetRoman(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestInputIntNumber2()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            RomanNumber result = builder.GetRoman(-1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestInputIntNumber3()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            RomanNumber result = builder.GetRoman(0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestInputIntNumber4()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            RomanNumber result = builder.GetRoman(-548);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestInputIntNumber5()
        {
            ClassConfigData data = new ClassConfigData
            {
                IsUpperCase = true
            };
            var builder = new RomanBuilder(data);
            RomanNumber result = builder.GetRoman(4000);
        }


    }
}

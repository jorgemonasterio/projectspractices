﻿using Fundacion.Jala.Calculator.Operators;
using System;
using System.Collections;

namespace Fundacion.Jala.Calculator.BuildingTree
{
    public class Tree
    {
        public string PostFix;
        public Tree(string postFix)
        {
            this.PostFix = postFix;
        }
        public Tree()
        {

        }
        public Node ConstructTreeOther()
        {
            OperatorsData operatorData = new OperatorsData();
            Stack stackTree = new Stack();
            Node node, node1, node2;
            for (int i = 0; i < PostFix.Length; i++)
            {
                if (!operatorData.IsOperator(PostFix[i]))
                {
                    node = new Node(PostFix[i]);
                    stackTree.Push(node);
                }
                else
                {
                    node = new Node(PostFix[i]);

                    node1 = (Node)stackTree.Pop();
                    node2 = (Node)stackTree.Pop();

                    node.RightData = node1;
                    node.LeftData = node2;

                    stackTree.Push(node);
                }
            }
            node = (Node)stackTree.Peek();
            stackTree.Pop();
            return node;
        }

        public Node ConstructTree(Queue operators, Queue operands)
        {
            Stack stackTree = new Stack();
            int lengthTree = operators.Count;
            Node node, node1, node2;
            for (int i = 0; i < lengthTree; i++)
            {
                node = new Node((char)operands.Dequeue());
                stackTree.Push(node);
                if (i == 0)
                {
                    node = new Node((char)operands.Dequeue());
                    stackTree.Push(node);
                }
                node = new Node((char)operators.Dequeue());

                node1 = (Node)stackTree.Pop();
                node2 = (Node)stackTree.Pop();

                node.RightData = node1;
                node.LeftData = node2;

                stackTree.Push(node);
            }
            node = (Node)stackTree.Peek();
            stackTree.Pop();
            return node;
        }
    }
}

const axios = require('axios');

async function getInformation(){
    try {
        const promises = [];
        const response = await axios.get('https://api.github.com/users');
        response.data.forEach(element => {
            promises.push(axios.get(`https://api.github.com/users/${element.login}`))
        });
        Promise.all(promises)
        .then(response => {
            const users = response.map(element => {
                return {
                    id: element.data.id,
                    picture: element.data.avatar_url,
                    username: element.data.login,
                    type: element.data.type,
                    followersLength: element.data.followers,
                    followingLength: element.data.following,
                    ulr: element.data.url,
                }
            });
            console.log(users);
        })
        .catch(error => {
            console.log(error.message);
        });
    } catch (error) {
        console.log(error.message);
    }
};

getInformation();
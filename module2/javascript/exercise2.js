const object = {
    vocals : ['a','e','i','o','u'],
    results : [],
    vocalCounter : function(word) 
    {
        let vocalCounter = 0;
        for (let index = 0; index < word.length; index++) 
        {
            const char = word[index];
            if(this.charContain(char))
            {
                vocalCounter++;
            }
        }
        this.results.push(`word: ${word} has ${vocalCounter}`);
        return vocalCounter;
    },

    charContain: function(char)
    {
        return this.vocals.some(vocal => vocal === char);
    }

};

object.vocalCounter('dog');
console.log(object.results);

object.vocalCounter('computer');
console.log(object.results);

object.vocalCounter('entity relation');
console.log(object.results);

﻿using System.Text;

namespace VectorClassLibrary.NetFramework
{
    public class Vector
    {
        int x;
        int y;
        int length;
        public Vector(int x1, int y1)
        {
            x = x1;
            length++;
            y = y1;
            length++;
        }

        public Vector SumOperation(Vector vector2)
        {
            return new Vector(x+vector2.x,y+vector2.y);
        }
        public int DotOperation(Vector vector2)
        {
            var result = ((x * vector2.x) + (y * vector2.y));
            return result;
        }
        public int LenghtVector()
        {
            return length;
        }

        public override string ToString()
        {
            StringBuilder vector = new StringBuilder();
            vector.Append("[").Append(x).Append(",").Append(y).Append("]");
            return vector.ToString();
        }

        public int getXcoordenate()
        {
            return x;
        }

        public int getYcoordenate()
        {
            return y;
        }
    }
}

﻿using Fundacion.Jala.DevInt.Practice2.Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fundacion.Jala.DevInt.Practice2.Store
{
    public class StockManager
    {
        private IList<object> _products;
        private IDictionary<Guid, int> _stockByProductId;
        public StockManager()
        {
            _products = new List<object>();
            _stockByProductId = new Dictionary<Guid, int>();
        }
        public void Add(object product, int quantity)
        {
            var productItem = product as ProductBase;
            if (productItem == null)
            {
                return;
            }
            if (!_stockByProductId.ContainsKey(productItem.Id))
            {
                _products.Add(productItem);
                _stockByProductId.Add(productItem.Id, 0);
                _stockByProductId[productItem.Id] += quantity;
            }
            else
            {
                _stockByProductId[productItem.Id] += quantity;
            }    
        }
        
        public void Add(Guid productId, int quantity)
        {
            if (_stockByProductId.ContainsKey(productId))
            {
                _stockByProductId[productId] += quantity;
            }
        }

        public void Remove(Guid productId)
        {
            if (_stockByProductId.ContainsKey(productId))
            {
                _stockByProductId.Remove(productId);
            }
        }

        public void Sell(Guid productId, int quantityToSell = 1)
        {
            if (_stockByProductId.ContainsKey(productId))
            {
                _stockByProductId[productId] -= quantityToSell;
            }
        }

        public IEnumerable<object> GetProductsBy(string type, string productFullName)
        {
            var products = new List<object>();
            string[] names = productFullName.Split('*');
            foreach (var item in _products)
            {
                var typeProduct = item.GetType().ToString();
                if (typeProduct == "Fundacion.Jala.DevInt.Practice2.Store.Models.Monitor")
                {
                    var nameProduct = (item as Monitor).GetName();
                    if (nameProduct == productFullName)
                    {
                        var monitorId = (item as Monitor).Id;
                        if (_stockByProductId.ContainsKey(monitorId))
                        {
                            products.Add(_stockByProductId[monitorId]);
                        }
                        return products;
                    }
                }
                else if (typeProduct == "Fundacion.Jala.DevInt.Practice2.Store.Models.GameController")
                {
                    var nameProduct = (item as GameController).FullName;
                    foreach (var nameItem in names)
                    {
                        if (nameItem == nameProduct)
                        {
                            var gameControllerId = (item as GameController).Id;
                            if (_stockByProductId.ContainsKey(gameControllerId))
                            {
                                products.Add(_stockByProductId[gameControllerId]);
                            }
                        }
                    }
                }
            }
            return products;
        }

        public IEnumerable<object> GetProductsBy(string productType)
        {
            var name = GetProductName(productType);
            return GetProductsBy(productType, name);
        }

        public string GetProductName(string productType)
        {
            StringBuilder builder = new StringBuilder();
            var product = $"Fundacion.Jala.DevInt.Practice2.Store.Models.{productType}";
            foreach (var item in _products)
            {
                var type = item.GetType().ToString();
                if (product == type && productType != "GameController")
                {
                    builder.Append((item as Monitor).FullName);
                }
                else if (product == type && productType != "Monitor")
                {
                    builder.Append((item as GameController).FullName).Append("*");
                }
            }
            return builder.ToString();
        }

        public int GetStockFor(Guid productId)
        {
            var quantity = 0;
            if (_stockByProductId.ContainsKey(productId))
            {
                quantity = _stockByProductId[productId];
            }
            return quantity;
        }

        public int GetStockFor(string productType)
        {
            var name = GetProductName(productType);
            return GetStockFor(productType, name);
        }

        public int GetStockFor(string productType, string productFullName)
        {
            int value = 0;
            string[] names = productFullName.Split('*');
            foreach (var item in _products)
            {
                var type = item.GetType().ToString();
                if (type == "Fundacion.Jala.DevInt.Practice2.Store.Models.Monitor")
                {
                    var nameProduct = (item as Monitor).FullName;
                    if (nameProduct == productFullName)
                    {
                        var monitorId = (item as Monitor).Id;
                        if (_stockByProductId.ContainsKey(monitorId))
                        {
                            value += _stockByProductId[monitorId];
                        }
                        return value;
                    }
                }
                else if(type == "Fundacion.Jala.DevInt.Practice2.Store.Models.GameController")
                {
                    var nameProduct = (item as GameController).FullName;
                    foreach (var nameItem in names)
                    {
                        if (nameItem == nameProduct)
                        {
                            var gameControllerId = (item as GameController).Id;
                            if (_stockByProductId.ContainsKey(gameControllerId))
                            {
                                value += _stockByProductId[gameControllerId];
                            }
                        }
                    }
                }
            }
            if (value < 0) value = 0;
            return value;
        }
    }
}

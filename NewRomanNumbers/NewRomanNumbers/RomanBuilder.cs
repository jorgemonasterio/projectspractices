﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRomanNumbers
{
    public class RomanBuilder
    {
        Parser parseNumber;
        ClassConfigData classConfigData;
        const string message = "The numbers only goes till 3999";
        const string messageZero = "The number cant be 0";
        const string messageNegative = "The numbers cant be negative";

        public RomanBuilder()
        {
            parseNumber = new Parser();
            classConfigData = new ClassConfigData();
            classConfigData.ConfigurationAppSettings();
        }
        public RomanBuilder(ClassConfigData classConfigData)
        {
            parseNumber = new Parser();
            this.classConfigData = classConfigData;
        }
        public RomanNumber GetRoman(int value)
        {
            if (value == 0) { throw new ArgumentException(messageZero); };
            if (value < 0) { throw new ArgumentException(messageNegative); };
            StringBuilder builder = new StringBuilder();
            var thousand = value / 1000;
            if (thousand > 3) { throw new ArgumentException(message); };
            int hundred = (value - (thousand * 1000)) / 100;
            int ten = (value - (thousand * 1000 + hundred * 100)) / 10;
            int unity = value - (thousand * 1000 + hundred * 100 + ten * 10);
            var itemU = parseNumber.GetItem(unity * 1);
            var itemD = parseNumber.GetItem(ten * 10);
            var itemC = parseNumber.GetItem(hundred * 100);
            var itemM = parseNumber.GetItem(thousand * 1000);
            var stringValue = " ";
            builder.Append(itemM).Append(itemC).Append(itemD).Append(itemU);
            stringValue = builder.ToString();
            string finalNumber = BuildNumberWithProperties(stringValue);
            return new RomanNumber(finalNumber);
        }

        private string BuildNumberWithProperties(string number)
        {
            List<string> properties = classConfigData.GetProperties();
            if (!classConfigData.IsUpperCase && !classConfigData.HaveBrackets)
            {
                return number.ToLower();
            }
            else if (classConfigData.IsUpperCase && !classConfigData.HaveBrackets)
            {
                return  number;
            }
            else if (!classConfigData.IsUpperCase && classConfigData.HaveBrackets)
            {
                return (properties.Find(item => item == "[") + number.ToLower() + properties.Find(item => item == "]"));
            }
            else if (classConfigData.IsUpperCase && classConfigData.HaveBrackets)
            {
                return (properties.Find(item => item == "[") + number + properties.Find(item => item == "]"));
            }
            return " ";
        }
    }
}

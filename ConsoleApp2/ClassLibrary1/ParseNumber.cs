﻿using Fundacion.Jala.RomanNumbers.RomanNumber;
using Fundacion.Jala.RomanNumbers.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1
{
    public class ParseNumber
    {
        StringBuilder builderRoman;
        public ParseNumber()
        {
            builderRoman = new StringBuilder();
        }
        public RomanNumber ParseToRomanNumber(int number)
        {
            int numberArabig = number;
            foreach (var item in Storage.romanValueMap.Reverse())
            {
                while (number >= item.Key)
                {
                    number -= item.Key;
                    //Console.WriteLine(number);
                    builderRoman.Append(item.Value);
                }
            }
            return new RomanNumber (numberArabig, builderRoman.ToString());
        }

    }
}

﻿using System;

namespace Fundacion.Jala.Calculator.Operators
{
    public class OperatorsData
    {
        public const string OperatorsValue = "*/+-";

        public bool IsOperator(char data)
        {
            for (int i = 0; i < OperatorsValue.Length; i++)
            {
                if (OperatorsValue[i] == data)
                {
                    return true;
                }
            }
            return false;
        }
        public bool isDivision(char data)
        {
            return data == '/';
        }
        public bool isMultiplication(char data)
        {
            return data == '*';
        }
    }
}

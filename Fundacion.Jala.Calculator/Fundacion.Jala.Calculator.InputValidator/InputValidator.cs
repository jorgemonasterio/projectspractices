﻿using Fundacion.Jala.Calculator.Operators;
using System.Text.RegularExpressions;
using System;

namespace Fundacion.Jala.Calculator.InputValidator
{
    public class Validator
    {
        public bool correctFormatExpression(string expression)
        {
            return !doubleOperator(expression) && !thereAreLetters(expression) && !firstInvalidCharacter(expression);
        }
        private bool doubleOperator(string expression)
        {
            OperatorsData operators = new OperatorsData();
            for (int i = 0; i < expression.Length; i++)
            {
                if (i != 0 && operators.IsOperator(expression[i]))
                {
                    if (operators.IsOperator(expression[i - 1]))
                        return true;
                }
            }
            return false;
        }
        private bool thereAreLetters(string expression)
        {
            Regex rgx = new Regex(@"[a-zA-Z]");
            for (int i = 0; i < expression.Length; i++)
            {
                if(rgx.IsMatch(expression))
                {
                    return true;
                }
            }
            return false;
        }
        private bool firstInvalidCharacter(string expression)
        {
            OperatorsData operators = new OperatorsData();
            if (expression.Length > 1)
                return operators.isDivision(expression[0]) || operators.isMultiplication(expression[0]);
            return false;
        }
    }
}

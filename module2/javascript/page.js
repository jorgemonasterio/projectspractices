class User {
    constructor(username, passwd){
        this.id = 'a'+ username +1;
        this.username = username;
        this.passwd = passwd;
        this.bitacoreOnEachAction = [];
        this.privileges = [];
    }
    login(){
        let logged = new Date();
        console.log('user had been logged');
        this.bitacoreOnEachAction.push({userID: this.id, user : this.username ,message: 'user had been logged', date: logged});
    }
    logout(){
        let noLogged = new Date();
        console.log('user is no longer connect!');
        this.bitacoreOnEachAction.push({userID: this.id, user : this.username, message: 'user is no longer connect', date: noLogged});
    }
    saveBitacoreOnEachAction(){
        console.log(this.bitacoreOnEachAction);
    }
    askForPrivileges()
    {
        if(this.privileges.length > 0){
            console.log('you have the following privileges:' + ' ' + this.privileges);
        }else{
            console.log('you dont have any privileges contact the admin!');
        }
    }
}

class Visitor extends User{
    constructor(username,passwd){
        super(username,passwd);
    }
    visitPage(){
        let visited = new Date();
        console.log('visitor is visiting a page');
        this.bitacoreOnEachAction.push({user : this.username, message: 'user is no longer connect', date: visited});
    }
}
class Administrator extends User{
    constructor(username,passwd){
        super(username,passwd);
        this.users = [];
    }
    addUser(user){
        let created = new Date();
        this.users.push({userID: user.id, username: user.username});
        this.bitacoreOnEachAction.push({Admin : this.username, message: user.username + ' ' + 'user was added', date: created});
    }
    deleteUser(username){
        let deleted = new Date();
        for (let index = 0; index < this.users.length; index++) {
            if(username === this.users[index].username){
                this.users.splice(index,1);
                break;
            }
        }
        this.bitacoreOnEachAction.push({Admin : this.username, message: username+ ' '+ 'user was deleted', date: deleted});
    }
    setPrivileges(username, privilege){
        let date = new Date();
        for (let index = 0; index < this.users.length; index++) {
            if(username === this.users[index].name){
                this.users[index].privileges.push(privilege);
                console.log(this.users[index].privileges);
                break;
            }
        }
        this.bitacoreOnEachAction.push({Admin : this.username, message: username+' '+'user had the privilege:' + privilege, date: date});
    }
}

var user1 = new User('mario',234);
var user2 = new User('luigi',234);
var user3 = new User('peach',234);

var admin = new Administrator('pepe',123);
admin.login();
admin.addUser(user1);
admin.addUser(user2);
admin.addUser(user3);
admin.deleteUser(user1.username);
admin.setPrivileges('peach','write');
user1.askForPrivileges();
admin.saveBitacoreOnEachAction();
admin.logout();
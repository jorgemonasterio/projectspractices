﻿using System;

namespace NewRomanNumbers
{
    public class RomanNumber
    {
        public string Symbol { set; get; }
        public int Value { set; get; }
        public RomanNumber(int value)
        {
            Value = value;
        }
        public RomanNumber(string symbol)
        {
            Symbol = symbol;
        }
        public string ShowRomanNumber()
        {
            return $"roman number: {Symbol}";
        }
    }
}

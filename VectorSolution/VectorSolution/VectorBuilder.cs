﻿using System;
using VectorClassLibrary.NetFramework;

namespace VectorSolution
{
    public class VectorBuilder
    {
        const string messageError = "the input is not a valid number for the vector";
        const string messageInputError = "The index that you're trying to entry its not valid";
        public Vector ConstructVector(string value)
        {
            var entries = new Vector(0, 0);
            if (ValidateVectorEntry(value))
            {
                string[] words = value.Split(',');
                int[] arr = new int[2];
                int i = 0;
                foreach (string word in words)
                {
                    arr[i] = ParseToInt(word);
                    i++;
                }
                entries = new Vector(arr[0], arr[1]);
            }
            return entries;
        }
        public int ParseToInt(string word)
        {
            if (!int.TryParse(word, out var result))
            {
                throw new ArgumentException(messageError);
            }
            return result;
        }
        public bool ValidateVectorEntry(string value)
        {
            string[] words = value.Split(',');
            if (words.Length == 2)
            {
                return true;
            }
            else
            {
                throw new ArgumentException(messageInputError);
            }
        }
    }
}

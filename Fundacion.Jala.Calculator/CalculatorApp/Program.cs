﻿using Fundacion.Jala.Calculator.Exception;
using Fundacion.Jala.Calculator.PostFixCoverter;
using Fundacion.Jala.Calculator.WorkingSuffix;
using Fundacion.Jala.Calculator.ExecuterOperation;
using System;

namespace CalculatorApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                PostFixConverter converter = new PostFixConverter();
                var PostFixQueue = converter.GenerateFinalPostFix("5+3*2*2"); //returns a String
                var executeOperation = new ExecuterOperation();
                //string postFixAux = "10510*3++3";
                Console.WriteLine(PostFixQueue);
                executeOperation.StartOperation("134*1123**5++++"); //1532151+++++
                //Console.WriteLine(PostFixQueue);
                //string postFix = "52*13*+2-";
                //GenerateStack newStack = new GenerateStack(PostFixQueue);
                //newStack.BuildingStacks();
                Console.ReadKey();
            }
            catch(ExceptionHandler ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRomanNumbers
{
    public static class ConversionTable
    {
        public static Dictionary<int, string> romanValueMapUnity = new Dictionary<int, string> {   { 1, "I"},
                                                                                                   { 5, "V"},
                                                                                                   { 10,"X" } };

        public static Dictionary<int, string> romanValueMapTen = new Dictionary<int, string> {     { 10, "X"},
                                                                                                   { 50, "L"},
                                                                                                   { 100,"C" } };

        public static Dictionary<int, string> romanValueMapHundred = new Dictionary<int, string> { { 100, "C"},
                                                                                                   { 500, "D"},
                                                                                                   { 1000,"M" } };
    }
}

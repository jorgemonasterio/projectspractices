﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRomanNumbers
{
    public class Parser
    {
        public Parser()
        {

        }
        public string GetItem(int value)
        {
            if (value >= 1000 && value < 4000)
            {
                var newValueM = GetItemM(value);
                return newValueM;
            }
            else if (value >= 100 && value < 1000)
            {
                var newValueC = GetItemC(value);
                return newValueC;
            }
            else if (value >= 10 && value < 100)
            {
                var newValueD = GetItemD(value);
                return newValueD;
            }
            else if (value >= 1 && value < 10)
            {
                var newValueU = GetItemU(value); //5
                return newValueU;
            }
            return "";
        }

        private string GetItemU(int value)
        {
            StringBuilder builder = new StringBuilder();
            if (value <= 3)
            {
                foreach (var item in ConversionTable.romanValueMapUnity.Reverse())
                {
                    while (value >= item.Key)
                    {
                        value -= item.Key;
                        builder.Append(item.Value);
                    }
                }
            }
            else if (value >= 5 && value != 9)
            {
                foreach (var item in ConversionTable.romanValueMapUnity.Reverse())
                {
                    while (value >= item.Key)
                    {
                        value -= item.Key;
                        builder.Append(item.Value);
                    }
                }
            }
            else if (value == 4)
            {
                builder.Append("IV");
            }
            else if (value == 9)
            {
                builder.Append("IX");
            }

            return builder.ToString();
        }

        private string GetItemD(int value)
        {
            StringBuilder builder = new StringBuilder();
            if (value <= 30)
            {
                foreach (var item in ConversionTable.romanValueMapTen.Reverse())
                {
                    while (value >= item.Key)
                    {
                        value -= item.Key;
                        //Console.WriteLine(number);
                        builder.Append(item.Value);
                    }
                }
            }
            else if (value >= 50 && value != 90)
            {
                foreach (var item in ConversionTable.romanValueMapTen.Reverse())
                {
                    while (value >= item.Key)
                    {
                        value -= item.Key;
                        //Console.WriteLine(number);
                        builder.Append(item.Value);
                    }
                }
            }
            else if (value == 40)
            {
                builder.Append("XL");
            }
            else if (value == 90)
            {
                builder.Append("XC");
            }

            return builder.ToString();
        }
        private string GetItemC(int value)
        {
            StringBuilder builder = new StringBuilder();
            if (value <= 300)
            {
                foreach (var item in ConversionTable.romanValueMapHundred.Reverse())
                {
                    while (value >= item.Key)
                    {
                        value -= item.Key;
                        builder.Append(item.Value);
                    }
                }
            }
            else if (value >= 500 && value != 900)
            {
                foreach (var item in ConversionTable.romanValueMapHundred.Reverse())
                {
                    while (value >= item.Key)
                    {
                        value -= item.Key;
                        builder.Append(item.Value);
                    }
                }
            }
            else if (value == 400)
            {
                builder.Append("CD");
            }
            else if (value == 900)
            {
                builder.Append("CM");
            }
            return builder.ToString();
        }
        private string GetItemM(int value)
        {
            StringBuilder builder = new StringBuilder();
            if (value <= 3000)
            {
                foreach (var item in ConversionTable.romanValueMapHundred.Reverse())
                {
                    while (value >= item.Key)
                    {
                        value -= item.Key;
                        builder.Append(item.Value);
                    }
                }
            }
            return builder.ToString();
        }
    }
}

function getUrl()
{
    var loc = document.location.href;
    if(loc.indexOf('?')>0)
    {
        var getString = loc.split('?')[1];
        var GET = getString.split('&');
        var get = {};
        for(var i = 0;  i < GET.length; i++){
            var tmp = GET[i].split('=');
            get[tmp[0]] = (tmp[1]);
        }
        return get;
    }
}

window.onload = function()
{
    var values=getUrl();
    if(values) {
        var body = document.getElementsByTagName("body")[0];
        var table = document.createElement("table");
        var tblhead = document.createElement("thead");
        var tblBody = document.createElement("tbody");
        var tags = ['Name','Lastname','Age','Gender','Job','Username','Language'];
        var threadHeaders = document.createElement("tr");
        var thread = document.createElement("tr");
        var counter = 0;
        for(var index in values){   
            var headerCell = document.createElement("th");
            var cell = document.createElement("td");
            var textHeader = document.createTextNode(tags[counter]);
            var textCell = document.createTextNode(values[index]);
            cell.appendChild(textCell);
            headerCell.appendChild(textHeader);
            threadHeaders.appendChild(headerCell);
            thread.appendChild(cell);
            tblhead.appendChild(threadHeaders);                                    
            tblBody.appendChild(thread);
            counter++;
        }
    }
    table.appendChild(tblhead);
    table.appendChild(tblBody);
    body.appendChild(table);
    table.setAttribute("border", "2");
}
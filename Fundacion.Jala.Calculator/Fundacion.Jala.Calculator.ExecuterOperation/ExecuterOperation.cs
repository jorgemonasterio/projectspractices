﻿using System;
using System.Collections;
using Fundacion.Jala.Calculator.BuildingTree;
using Fundacion.Jala.Calculator.WorkingSuffix;
namespace Fundacion.Jala.Calculator.ExecuterOperation
{
    public class ExecuterOperation
    {
        public void StartOperation(string postFix)
        {
            GenerateStack stackGenerator = new GenerateStack(postFix);
            Node _tree = stackGenerator.BuildingStacks();
            GenerateTreeStack(_tree);
        }
        private void GenerateTreeStack(Node _tree)
        {
            Stack auxStack = new Stack();
            while (_tree.LeftData != null)
            {
                ArrayList lista = new ArrayList();
                lista.Add(_tree.Value);
                lista.Add(_tree.RightData.Value);
                _tree = _tree.LeftData;
                if (_tree.LeftData == null)
                {
                    lista.Add(_tree.Value);
                    var p = lista[1];
                }
                auxStack.Push(lista);
            }
            IterateStack(auxStack);
        }
        private void IterateStack(Stack _stack)
        {
            int t = _stack.Count;
            int result = 0;
            for (int i = 0; i < t; i++)
            {
                var currentPos = (ArrayList)_stack.Pop();
                if (i == 0)
                {
                    result = MakeOperation((char)currentPos[0], (char)currentPos[1], (char)currentPos[2]);
                }
                else
                {
                    string stringNumber = currentPos[1].ToString();
                    result = MakeOperation((char)currentPos[0], int.Parse(stringNumber), result);
                }
            }
            Console.WriteLine($"Result is: {result}");
        }
        private int MakeOperation<T>(T _operator, T _operand1, T _operando2)
        {
            int result = 0;
            string auxOperand1 = _operand1.ToString();
            string auxOperando2 = _operando2.ToString();
            switch (Convert.ToInt32(_operator))
            {
                case 42:
                    result = Convert.ToInt32(auxOperand1) * Convert.ToInt32(auxOperando2);
                    break;
                case 43:
                    result = Convert.ToInt32(auxOperand1) + Convert.ToInt32(auxOperando2);
                    break;
                case 45:
                    result = Convert.ToInt32(auxOperando2) - Convert.ToInt32(auxOperand1);
                    break;
                case 47:
                    result = Convert.ToInt32(auxOperando2) / Convert.ToInt32(auxOperand1);
                    break;
            }
            return result;
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fundacion.Jala.Calculator.BuildingTree;
using Fundacion.Jala.Calculator.Operators;

namespace Fundacion.Jala.Calculator.WorkingSuffix
{
    public class GenerateStack
    {
        public string PostFix;
        public Queue OperatorsStack;
        public Queue OperandsStack;

        public GenerateStack(string postFix)
        {
            this.PostFix = postFix;
            this.OperatorsStack = new Queue();
            this.OperandsStack = new Queue();
        }
        public Node BuildingStacks()
        {
            OperatorsData operatorData = new OperatorsData();
            for (int i = 0; i < PostFix.Length; i++)
            {
                if (operatorData.IsOperator(PostFix[i]))
                {
                    OperatorsStack.Enqueue(PostFix[i]);
                }
                else
                {
                    OperandsStack.Enqueue(PostFix[i]);
                }

            }
            Tree newTree = new Tree();
            Node _tree = newTree.ConstructTree(OperatorsStack, OperandsStack);
            return _tree;
        }
    }
}
